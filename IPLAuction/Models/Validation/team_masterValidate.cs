﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace IPLAuction.Models.Validation
{
    public class team_masterValidate
    {
    }

    [MetadataType(typeof(team_masterMetaType))]
    public partial class team_master
    {

    }
    [MetadataType(typeof(team_masterMetaType))]
    public partial class team_budget
    {

    }

    public class team_masterMetaType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int team_id { get; set; }

        [DisplayName("Team Name*")]
        [Required(ErrorMessage = "Enter team name")]
        public string team_name { get; set; }

        [DisplayName("Team Short Code")]
        public string team_short_code { get; set; }

        [DisplayName("Active*")]
        [Required(ErrorMessage = "Select team status")]
        public Nullable<bool> active { get; set; }

        [DisplayName("Team Owner*")]
        [Required(ErrorMessage = "Select team owner")]
        public Nullable<int> owner_id { get; set; }

        [DisplayName("Founded On*")]
        [Required(ErrorMessage = "Date is required")]
        public Nullable<System.DateTime> team_founded { get; set; }

        [DisplayName("Team Captain*")]
        [Required(ErrorMessage = "Select team captain")]
        public Nullable<int> team_captain_player_id { get; set; }

        [DisplayName("Coach Name*")]
        [Required(ErrorMessage = "Enter coach name")]
        public Nullable<int> coach_id { get; set; }

        [DisplayName("Home City*")]
        [Required(ErrorMessage = "Select home city")]
        public Nullable<int> homecity { get; set; }

        [DisplayName("Home Ground*")]
        [Required(ErrorMessage = "Select home ground")]
        public Nullable<int> homeground { get; set; }

        [DisplayName("Team Logo*")]
        public byte[] team_logo { get; set; }

        public static implicit operator team_masterMetaType(team_master v)
        {
            throw new NotImplementedException();
        }
    }
}