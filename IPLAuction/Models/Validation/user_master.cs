﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IPLAuction.Models.Extended
{
    [MetadataType(typeof(user_masterMetaData))]
    public partial class user_master
    {
    }

    public class user_masterMetaData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int user_id { get; set; }

        [DisplayName("Useremail")]
        [Required(ErrorMessage = "Useremail is required")]
        public string user_email { get; set; }

        [DisplayName("Username")]
        [Required(ErrorMessage = "Username is required")]
        public string user_name { get; set; }

        [DisplayName("Password")]
        [Required(ErrorMessage = "Password is required")]
        public string user_password { get; set; }

        [DisplayName("Usertype")]
        [Required(ErrorMessage = "Select privilige level")]
        public string user_type { get; set; }

        [DisplayName("User Display Picture")]
        public string user_image { get; set; }
    }
}