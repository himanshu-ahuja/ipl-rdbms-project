﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace IPLAuction.Models.Validation
{
    public class match_masterValidate
    {
    }

    [MetadataType(typeof(match_masterMetaType))]
    public partial class match_master
    {

    }

    public class match_masterMetaType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int match_id { get; set; }

        [DisplayName("Match Date")]
        [Required(ErrorMessage = "Select match date")]
        public System.DateTime match_date { get; set; }

        [DisplayName("Select team")]
        [Required(ErrorMessage = "Team is required")]
        public int team_id { get; set; }

        [DisplayName("Select opponent team")]
        [Required(ErrorMessage = "Opponent team is required")]
        public int opponent_team_id { get; set; }

        [DisplayName("Season")]
        //[Required(ErrorMessage = "Season is required")]
        public Nullable<int> season_id { get; set; }

        [DisplayName("Select venue")]
        [Required(ErrorMessage = "Venue is required")]
        public int venue_id { get; set; }

        [DisplayName("Toss winner team")]
        [Required(ErrorMessage = "Toss winner team name is required")]
        public int toss_winner_team_id { get; set; }

        [DisplayName("Toss decision")]
        [Required(ErrorMessage = "Toss decision is required")]
        public string toss_decision { get; set; }

        [DisplayName("IS Superover?")]
        [Required(ErrorMessage = "Status is required")]
        public Nullable<bool> IS_Superover { get; set; }

        [DisplayName("Match result")]
        [Required(ErrorMessage = "Result type is required")]
        public bool IS_Result { get; set; }

        [DisplayName("Win Type")]
        [Required(ErrorMessage = "Win type is required")]
        public string win_type { get; set; }

        [DisplayName("Won By")]
        [Required(ErrorMessage = "Enter won by")]
        public Nullable<int> won_by { get; set; }

        [DisplayName("Match Winner")]
        [Required(ErrorMessage = "Match winner is required")]
        public Nullable<int> match_winner_team_id { get; set; }

        [DisplayName("Man of the Match")]
        [Required(ErrorMessage = "Man of the match is required")]
        public int man_match_player_id { get; set; }

        [DisplayName("Select Umpire 1")]
        [Required(ErrorMessage = "Umpire 1 is required")]
        public Nullable<int> first_umpire_id { get; set; }

        [DisplayName("Select Umpire 2")]
        [Required(ErrorMessage = "Umpire 2 is required")]
        public Nullable<int> second_umpire_id { get; set; }

        [DisplayName("City")]
        [Required(ErrorMessage = "City is required")]
        public int city_id { get; set; }

        //[DisplayName("Country")]
        //[Required(ErrorMessage = "Country is required")]
        public int country_id { get; set; }
    }
}