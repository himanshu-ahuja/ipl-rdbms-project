﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using System.Web.Mvc;

namespace IPLAuction.Models.Validation
{
    public class player_masterValidate
    {
    }

    [MetadataType(typeof(player_masterMetaData))]
    public partial class player_master
    {
       
    }

    [MetadataType(typeof(player_masterMetaData))]
    public partial class player_skills
    {

    }

    public class player_masterMetaData
    {

      
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int player_id { get; set; }

        [DisplayName("Player Name*")]
        [Required(ErrorMessage = "Enter player name")]
        public string player_name { get; set; }

        [DisplayName("Player DoB*")]
        [Required(ErrorMessage = "Select player's dob")]
        public System.DateTime player_dob { get; set; }

        [DisplayName("Batting Style*")]
        [Required(ErrorMessage = "Select batting style")]
        public Nullable<int> player_batting_style { get; set; }

        [DisplayName("Batting Star*")]
        [Required(ErrorMessage = "Select batting star")]
        public int player_batting_star { get; set; }

        [DisplayName("Bowling Skill")]
        [Required(ErrorMessage = "Select bowling skill")]
        public int player_bowling_skill { get; set; }

        [DisplayName("Bowling Star*")]
        [Required(ErrorMessage = "Select bowling star")]
        public int player_bowling_star { get; set; }

        [DisplayName("Nationality*")]
        [Required(ErrorMessage = "Select player's nationality")]
        public int country { get; set; }

        [DisplayName("Display Picture")]
        [Required(ErrorMessage = "Select player's image")]
        public byte[] player_image { get; set; }

        [DisplayName("Active")]
        [Required(ErrorMessage = "Select player status")]
        public Nullable<bool> active { get; set; }

        public static implicit operator player_masterMetaData(player_master v)
        {
            throw new NotImplementedException();
        }

       
    }

}