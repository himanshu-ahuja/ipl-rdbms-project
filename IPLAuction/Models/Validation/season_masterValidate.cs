﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;


namespace IPLAuction.Models.Validation
{
    public class season_masterValidate
    {
    }

    [MetadataType(typeof(season_masterMetaData))]
    public partial class Season_master
    {
      
    }

    public class season_masterMetaData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int season_id { get; set; }

        [DisplayName("Season Year")]
        [Required(ErrorMessage = "Select season year")]
        public short season_year { get; set; }

        [DisplayName("Purple Cap Holder")]
        [Required(ErrorMessage = "Select purple cap player")]
        public int purple_cap_player_id { get; set; }

        [DisplayName("Man of the Series")]
        [Required(ErrorMessage = "Select man of the series")]
        public int man_series_player_id { get; set; }

        [DisplayName("Total Sixes")]
        [Required(ErrorMessage = "Enter total sixes")]
        public Nullable<int> sixes_count { get; set; }

        [DisplayName("Total Catches Count")]
        [Required(ErrorMessage = "Enter total catches")]
        public Nullable<int> catches_count { get; set; }

        [DisplayName("Orange Cap Holder")]
        [Required(ErrorMessage = "Select orange cap player")]
        public int orange_cap_player_id { get; set; }

        [DisplayName("Host Country")]
        [Required(ErrorMessage = "Select host country")]
        public Nullable<int> country_id { get; set; }
    }
}