﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IPLAuction.Models;
using System.ComponentModel.DataAnnotations;


namespace IPLAuction.Models.Extended
{
    public class TeamUtility : Controller
    {
        IPLEntities ipl = new IPLEntities();

        //Populating cities in dropdown
        public List<SelectListItem> GetCity()
        {
            var city = ipl.city_master.ToList();
            List<SelectListItem> li = new List<SelectListItem>();

            foreach (var m in city)
            {
                li.Add(new SelectListItem { Text = m.city_name, Value = m.city_id.ToString() });
            }
            return li;
        }

        //Populating cities in dropdown
        public List<SelectListItem> GetCityHostWise()
        {
            var year = ipl.season_master.Max(a => a.season_year);
            var host = ipl.season_master.Where(s => s.season_year == year).Select(a => a.country_id).FirstOrDefault();
            var city = ipl.city_master.Where(c => c.country_id == host).ToList();
            List<SelectListItem> li = new List<SelectListItem>();

            foreach (var m in city)
            {
                li.Add(new SelectListItem { Text = m.city_name, Value = m.city_id.ToString() });
            }
            return li;
        }

        //populating owners in dropdown
        public List<SelectListItem> GetOwner()
        {
            var owner = ipl.user_master.ToList();
            List<SelectListItem> liowner = new List<SelectListItem>();


            foreach (var o in owner)
            {
                liowner.Add(new SelectListItem { Text = o.user_name, Value = o.user_id.ToString() });
            }
            return liowner;
        }

        //Populate venue dropdown based on selected city
        public List<SelectListItem> GetVenueList(int id)
        {

            var venue = ipl.venue_master.Where(x => x.city_id == id).ToList();
            List<SelectListItem> livenues = new List<SelectListItem>();

           
                foreach (var x in venue)
                {
                    livenues.Add(new SelectListItem { Text = x.venue_name, Value = x.venue_id.ToString() });
                }
           
            return livenues;
        }

        //fetching coaches
        public List<SelectListItem> GetCoaches()
        {
            var coach = ipl.coach_master.Where(a => a.coach_status == false).ToList();

            //var coach = ipl.coach_master.ToList();
            List<SelectListItem> licoaches = new List<SelectListItem>();
           
                foreach(var x in coach)
                {
                    licoaches.Add(new SelectListItem { Text = x.coach_name, Value = x.coach_id.ToString() });
                }
           
            return licoaches;
        }

        //fetch all coaches
        public List<SelectListItem> GetAllCoaches()
        {
            var coach = ipl.coach_master.ToList();

            //var coach = ipl.coach_master.ToList();
            List<SelectListItem> licoaches = new List<SelectListItem>();

            foreach (var x in coach)
            {
                licoaches.Add(new SelectListItem { Text = x.coach_name, Value = x.coach_id.ToString() });
            }

            return licoaches;
        }


        //fetching team players
            public List<SelectListItem> GetSquad(int id)
        {

            var player = new
            {
                data =
                     (from p in ipl.player_master
                      join squad in ipl.team_squad on p.player_id equals squad.player_id
                      where squad.team_id == id /*&& squad.season_id == 9*/
                      select new
                      {
                          p.player_name,
                          p.player_id
                      }).ToList()
            };

            List<SelectListItem> li = new List<SelectListItem>();

            foreach (var p in player.data)
            {
                li.Add(new SelectListItem { Text = p.player_name, Value = p.player_id.ToString() });
            }
            return li;
        }

        //get current season year
        public int GetCurrentSeason()
        {
            var season = ipl.season_master.Max(a=>a.season_year);
            var id = ipl.season_master.Where(a => a.season_year == season).FirstOrDefault();
            int s_id = id.season_id;
            return s_id;
        }

       
        //Get Current season host country
        public int GetCurrentSeasonHostCountry()
        {
            var season = ipl.season_master.Max(a => a.season_year);
            var id = ipl.season_master.Where(a => a.season_year == season).FirstOrDefault();
            int c_id = id.country_id;
            return c_id;
        }

        //fetching team players
        public List<SelectListItem> GetActiveTeamPlayers(int id)
        {
            int current_season_id = GetCurrentSeason();

            //int current_season_id = 2016;
            var player = new
            {
                data =
                    (from t in ipl.team_master
                     join s in ipl.team_squad on t.team_id equals s.team_id
                    // join season in ipl.season_master on s.season_id equals season.season_id
                     join s1 in ipl.player_master on s.player_id equals s1.player_id
                     where t.team_id == id || s.season_id == current_season_id

                     select new
                     {
                         s1.player_id,
                         s1.player_name
                     }).ToList()

            };

            int season_id = 2016;
            var player1 = new
            {
                data =
                      (from t in ipl.team_master
                       join s in ipl.team_squad on t.team_id equals s.team_id
                       // join season in ipl.season_master on s.season_id equals season.season_id
                       join s1 in ipl.player_master on s.player_id equals s1.player_id
                       where t.team_id == id || s.season_id == season_id

                       select new
                       {
                           s1.player_id,
                           s1.player_name
                       }).ToList()

            };

           
            List<SelectListItem> li = new List<SelectListItem>();

            if (player == null)
            {
                foreach (var p in player1.data)
                {
                    li.Add(new SelectListItem { Text = p.player_name, Value = p.player_id.ToString() });
                }
            }
            else
            {
                foreach (var p in player.data)
                {
                    li.Add(new SelectListItem { Text = p.player_name, Value = p.player_id.ToString() });
                }
            }
            return li;
        }

        //return count of matches played by each time
        //public List<int> GetActiveTeamMatchCount()
        //{

           
        //    var team_m = (from m1 in ipl.match_master
                          
        //                  join t1 in ipl.team_master on m1.team_id equals t1.team_id into t1_join
        //                  from t1 in t1_join.DefaultIfEmpty()
        //                  group new { m1, t1 } by new
        //                  {
        //                      m1.team_id,
        //                      t1.team_name
        //                  } into g
        //                  orderby
        //                    g.Key.team_id
        //                  select new
        //                  {
        //                      g.Key.team_id,
        //                      Team_name = g.Key.team_name,
        //                      Matches_Played = g.Count(p => p.m1.match_id != 0)
        //                  }).ToList();

        //    //matches played as opponent
        //    var opponent = (from m1 in ipl.match_master
        //                    join t1 in ipl.team_master on new { Opponent_team_id = m1.opponent_team_id } equals new { Opponent_team_id = t1.team_id } into t1_join
        //                    from t1 in t1_join.DefaultIfEmpty()
        //                    group new { m1, t1 } by new
        //                    {
        //                        m1.opponent_team_id,
        //                        t1.team_name
        //                    } into g
        //                    orderby
        //                      g.Key.opponent_team_id
        //                    select new
        //                    {
        //                        g.Key.opponent_team_id,
        //                        Team_name = g.Key.team_name,
        //                        Matches_Played = g.Count(p => p.m1.match_id != 0)
        //                    }).ToList();

        //    List<int> matches_count = new List<int>();

        //    var combined1 = team_m.Zip(opponent, (count, count1) => new { Team = count, Opponent = count1 });

        //    foreach (var l in combined1)
        //    {
        //        matches_count.Add(Convert.ToInt32((l.Opponent.Matches_Played + l.Team.Matches_Played)));
        //        //Console.WriteLine((l.Opponent.Matches_Played + l.Team.Matches_Played));
        //    }

        //    return matches_count;
        //}
    }
}
