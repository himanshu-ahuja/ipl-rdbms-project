﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPLAuction.Models.Extended
{
    public class MatchUtility: Controller
    {
        IPLEntities ipl = new IPLEntities();
        TeamUtility TeamUtil = new TeamUtility();

        public List<SelectListItem> GetTeam()
        {
            var team = ipl.team_master.ToList();
            List<SelectListItem> li = new List<SelectListItem>();
           
            foreach (var m in team)
            {
                li.Add(new SelectListItem { Text = m.team_name, Value = m.team_id.ToString() });
            }
            return li;
        }

        public List<SelectListItem> GetActiveTeam()
        {
            var team = ipl.team_master.Where(t => t.active == true).ToList();
            List<SelectListItem> li = new List<SelectListItem>();

            foreach (var m in team)
            {
                li.Add(new SelectListItem { Text = m.team_name, Value = m.team_id.ToString() });
            }
            return li;
        }

        public List<SelectListItem> GetOpponent(int id)
        {

            var opponent = ipl.team_master.Where(x => x.team_id != id && x.active == true).ToList();
            List<SelectListItem> liopponents = new List<SelectListItem>();

            //liopponents.Add(new SelectListItem { Text = "--Select Opponent Team--", Value = "" });

            if (opponent != null)
            {
                foreach (var x in opponent)
                {
                    liopponents.Add(new SelectListItem { Text = x.team_name, Value = x.team_id.ToString() });
                }
            }
            return liopponents;
        }

      public List<SelectListItem> GetTossWinner(int id, int oppo)
        {
            var toss = ipl.team_master.Where(x => x.team_id == id || x.team_id == oppo).ToList();
            List<SelectListItem> litoss = new List<SelectListItem>();

                foreach(var x in toss)
                {
                    litoss.Add(new SelectListItem { Text = x.team_name, Value = x.team_id.ToString() });
                }
            
            return litoss;
        }


        public List<SelectListItem> GetUmpire()
        {
            var umpire = ipl.umpire_master.ToList();
            List<SelectListItem> li = new List<SelectListItem>();
          
            foreach ( var u in umpire)
            {
                li.Add(new SelectListItem { Text = u.umpire_name, Value = u.umpire_id.ToString() });
            }
            return li;
        }

        
        public List<SelectListItem> GetSecondUmpire(int id)
        {
            var umpire = ipl.umpire_master.Where(x => x.umpire_id!= id).ToList();
            List<SelectListItem> li = new List<SelectListItem>();

            foreach (var u in umpire)
            {
                li.Add(new SelectListItem { Text = u.umpire_name, Value = u.umpire_id.ToString() });
            }
            return li;
        }

      //public List<SelectListItem> GetSeasonWiseCities()
      //  {
      //      int season_id = TeamUtil.GetCurrentSeason();
      //      var host = ipl.season_master.Where(s => s.season_id == season_id).Select(h=>h.country_id);
      //      List<SelectListItem> li = new List<SelectListItem>();
      //  }

    }
}