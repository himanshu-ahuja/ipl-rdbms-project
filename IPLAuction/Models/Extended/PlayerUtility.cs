﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IPLAuction.Models;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace IPLAuction.Models.Extended
{
    public class PlayerUtility: Controller
    {
        IPLEntities ipl = new IPLEntities();

        //populate all players
        public List<SelectListItem> GetActivePlayers()
        {

            var players = ipl.player_master.ToList();
            List<SelectListItem> li = new List<SelectListItem>();

            foreach (var b in players)
            {
                li.Add(new SelectListItem { Text = b.player_name, Value = b.player_id.ToString() });
            }
            return li;
        }

        //populate active players
        public List<SelectListItem> GetAllActivePlayers()
        {

            var players = ipl.player_master.Where(p=>p.active == true).ToList();
            List<SelectListItem> li = new List<SelectListItem>();

            foreach (var b in players)
            {
                li.Add(new SelectListItem { Text = b.player_name, Value = b.player_id.ToString() });
            }
            return li;
        }

        //populate bowling skills in dropdown
        public List<SelectListItem> GetBowlingSkills()
        {
            
            var bowling = ipl.bowling_skill_master.ToList();
            List<SelectListItem> li = new List<SelectListItem>();
          
            foreach (var b in bowling)
            {
                li.Add(new SelectListItem { Text = b.bowling_skill, Value = b.bowling_skill_id.ToString() });
            }
            return li;
        }

        //populate batting skills in dropdown
        public List<SelectListItem> GetBattingStyle()
        {
            var batting = ipl.batting_style_master.ToList();
            List<SelectListItem> li = new List<SelectListItem>();
           // li.Add(new SelectListItem());

            foreach (var b in batting)
            {
                li.Add(new SelectListItem { Text = b.batting_style, Value = b.batting_id.ToString() });
            }
            return li;
        }

        //populate nationality in dropdown
        public List<SelectListItem> GetCountryList()
        {
            var country = ipl.country_master.ToList();
            List<SelectListItem> li = new List<SelectListItem>();
          //  li.Add(new SelectListItem());

            foreach (var c in country)
            {
                li.Add(new SelectListItem { Text = c.country_name, Value = c.country_id.ToString() });
            }
            return li;
        }

        //populate nationality in dropdown without pakistan
        public List<SelectListItem> GetCountryListForHost()
        {
            var country = ipl.country_master.Where(c=>c.country_id != 9).ToList();
            List<SelectListItem> li = new List<SelectListItem>();
            //  li.Add(new SelectListItem());

            foreach (var c in country)
            {
                li.Add(new SelectListItem { Text = c.country_name, Value = c.country_id.ToString() });
            }
            return li;
        }



    }
}