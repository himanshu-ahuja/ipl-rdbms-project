﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPLAuction.Models.Extended
{
    public class urlContent: Controller
    {
        public void urlDamnFix()
        {
            int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
            if (id != 0)
            {
                ViewBag.user_id = id;

                string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                ViewBag.user_name = user_name;
                TempData["user_name"] = ViewBag.user_name;
                TempData.Keep("user_name");

                string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                ViewBag.user_type = user_type;
                TempData["user_type"] = ViewBag.user_type;
                TempData.Keep("user_type");

                ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                TempData["user_image"] = ViewBag.user_image;
                TempData.Keep("user_image");
            }
        }
    }
}