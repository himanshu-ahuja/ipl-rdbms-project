﻿using IPLAuction.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;

namespace IPLAuction.Controllers.Master
{
    public class LoginController : Controller
    {
      
        IPLEntities ipl = new IPLEntities();
        public ActionResult IPL_Login()
        {


            if (Session["userID"] == null) {
                //TempData["login_success"] = true;
                Response.Cache.SetNoStore();
                RedirectToAction("IPL_Login", "Login");
            }
            TempData["login_success"] = true;
            TempData["team_login_success"] = true;
            //if(Session["userID"] != null)
            //{
            //    return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
            //}

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IPL_Login(string txtEmail, string txtPassword)
        {
            string email = "dummy";
            string pwd = "dummy";
            if (ModelState.IsValid)
            {

                email = Request.Form["txtEmail"];
                pwd = Request.Form["txtPassword"];
                pwd = pwd.Trim();

                var log = ipl.user_master.Where(a => a.user_email.Equals(email) && a.user_password.Equals(pwd)).FirstOrDefault();

                if (log != null)
                {

                    var team = ipl.team_master.Where(t => t.owner_id == log.user_id).FirstOrDefault();
                        if(team != null) { 
                                if(team.active == false)
                            {
                                TempData["team_login_success"] = false;
                                TempData["login_success"] = true;
                                return View();
                            }
                    }

                    Session["userID"] = log.user_id;
                    Session["user_name"] = log.user_name;
                    Session["user_type"] = log.user_type;
                    Session["user_email"] = log.user_email;

                    //var img = Convert.ToBase64String(log.user_image);
                    //var imgSrc = String.Format("data:image/gif;base64,{0}", base64);
                    var imgSrc = log.user_image;
                    if(imgSrc == null)
                    {
                        imgSrc = "/uploads/default-user-image.png";
                        Session["user_image"] = imgSrc;
                    }

                    Session["user_image"] = imgSrc;

                    if (log.user_type == "Admin")
                    {
                        return RedirectToAction("Admin_home", "home", new { area = "" });
                    }
                    else
                    {

                        return RedirectToAction("Team", "TeamHome", new { area = "" });
                    }
                }

                else
                {
                    TempData["login_success"] = false;
                    TempData["team_login_success"] = true;
                }

            }
            return View();
        }

       
        [HttpPost]
        public ActionResult ForgotPassword(string txtEmail)
        {

            var email = ipl.user_master.Where(a => a.user_email == txtEmail).FirstOrDefault();
            if(email == null)
            {
                TempData["message"] = "Email is not registered!";
                return RedirectToAction("IPL_Login", "Login");
            }

            try
            {
                if (ModelState.IsValid)
                {
                    var senderemail = new MailAddress("ahujaheman@gmail.com", "ahujaheman@gmail.com");
                    var receiveremail = new MailAddress("ahujaheman@gmail.com", "ahujaheman@gmail.com");

                    var password = "excision@#941";
                    var sub = "IPL Auction Forgot Password";
                    var body = "You have requested for password reset. Due to security reasons, we do not provide direct link for changing password. Please contact Administration team for further assitance";

                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(senderemail.Address, password)

                    };
                    using (var mess = new MailMessage(senderemail, receiveremail)
                    {
                        Subject = sub,
                        Body = body
                    }
                        )
                    {
                        smtp.Send(mess);
                    }
                }
                TempData["success"] = "Email Sent!";
            }
            catch (Exception)
            {
                TempData["message"] = "Email is not registered!";
            }
            return RedirectToAction("IPL_Login","Login");
        }
    }
}