﻿using IPLAuction.Models;
using IPLAuction.Models.Extended;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPLTrial.Controllers.Master_Controllers
{
    public class TeamSquadController : Controller
    {
        IPLEntities ipl = new IPLEntities();
        TeamUtility team_Util = new TeamUtility();
        

        public ActionResult TeamSquad()
        {

            TempData.Keep("team_id");
            TempData.Keep("team_succcess");
            TempData.Keep("team_logo");
            Response.Cache.SetNoStore();
           
            using (var context = new IPLEntities())
            {
                int u_id = 0;
                ViewBag.count_matches = context.match_master.ToList().Count;
                ViewBag.count_players = context.player_master.ToList().Count;
                ViewBag.count_teams = context.team_master.ToList().Count;

                u_id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
                if (u_id != 0)
                {
                    ViewBag.user_id = u_id;

                    string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                    ViewBag.user_name = user_name;
                    TempData["user_name"] = ViewBag.user_name;

                    string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                    ViewBag.user_type = user_type;
                    TempData["user_type"] = ViewBag.user_type;

                    ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                    TempData["user_image"] = ViewBag.user_image;

                }
            } //using ends

            int id = (int)TempData["team_id"];

            TempData["team_name"] = ipl.team_master.Where(t => t.team_id == id).Select(t => t.team_name).FirstOrDefault();
            TempData.Keep("team_name");
            var image = ipl.team_master.Where(t => t.team_id == id).Select(t => t.team_logo).FirstOrDefault();

            //if (image != null)
            //{
            //    ViewBag.team_logo = image;
            //}
            return View();
        }

        //Get Players
        public ActionResult GetPlayers()
        {

            ipl.Configuration.ProxyCreationEnabled = false;

            var player = new
            {
                data =

                 (from p in ipl.player_master
                
                  join c in ipl.country_master on p.country equals c.country_id
                  join bat in ipl.batting_style_master on p.player_batting_style equals bat.batting_id
                  join bowl in ipl.bowling_skill_master on p.player_bowling_skill equals bowl.bowling_skill_id
                  join skill in ipl.player_skills on p.player_id equals skill.player_id

                  where p.selected == false

                  select new
                  {
                      p.player_id,
                      p.player_name,
                      p.player_dob,
                      batting_id = bat.batting_id.ToString() == null ? String.Empty : bat.batting_style,
                      bowling_id = bowl.bowling_skill_id.ToString() == null ? String.Empty : bowl.bowling_skill,
                      batting_star = skill.player_skill_id.ToString() == null ? 0 : skill.player_batting_star,
                      bowling_star = skill.player_skill_id.ToString() == null ? 0 : skill.player_bowling_star,
                      c.country_name
                  }).ToList()
            };
            return Json(player, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Finalize(List<int> id)
        {

            int s_id = team_Util.GetCurrentSeason();
            var team = ipl.team_master.OrderByDescending(t => t.team_id).FirstOrDefault();
            int t_id = team.team_id;

            if (id.Count < 2)
            {
                ViewBag.selection_failure = true;
                return RedirectToAction("TeamSquad", "TeamSquad");
            }

            foreach (int pID in id)
            {
                team_squad squad = new team_squad();
                player_master player = new player_master();
                squad.player_id = pID;
                squad.team_id = t_id;
                squad.season_id = s_id;
                ipl.team_squad.Add(squad);

                var query = ipl.player_master.Where(p => p.player_id == pID).FirstOrDefault();
                query.selected = true;
            }
            ipl.SaveChanges();

            return RedirectToAction("AddNewTeam", "NewTeam");
        }

    }
}