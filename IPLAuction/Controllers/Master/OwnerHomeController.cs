﻿using IPLAuction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPLTrial.Controllers
{
    public class OwnerHomeController : Controller
    {
        // GET: OwnerHome
        public ActionResult TeamHome()
        {
            Response.Cache.SetNoStore();

            using (var context = new IPLEntities())
            {

                ViewBag.count_matches = context.match_master.ToList().Count;
                ViewBag.count_players = context.player_master.ToList().Count;
                ViewBag.count_teams = context.team_master.ToList().Count;

                int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
                if (id != 0)
                {
                    ViewBag.user_id = id;

                    string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                    ViewBag.user_name = user_name;
                    TempData["user_name"] = ViewBag.user_name;

                    string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                    ViewBag.user_type = user_type;
                    TempData["user_type"] = ViewBag.user_type;

                    ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                    TempData["user_image"] = ViewBag.user_image;

                }
            }

            return View();
        }

    }
}