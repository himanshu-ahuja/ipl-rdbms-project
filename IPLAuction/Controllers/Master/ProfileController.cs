﻿using IPLAuction.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPLTrial.Controllers.Master_Controllers
{
    public class ProfileController : Controller
    {
        IPLEntities ipl = new IPLEntities();

        [HttpGet]
        public ActionResult ProfileView()
        {
            Response.Cache.SetNoStore();

            using (var context = new IPLEntities())
            {

                ViewBag.count_matches = context.match_master.ToList().Count;
                ViewBag.count_players = context.player_master.ToList().Count;
                ViewBag.count_teams = context.team_master.ToList().Count;

                int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
                if (id != 0)
                {
                    ViewBag.user_id = id;

                    string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                    ViewBag.user_name = user_name;
                    TempData["user_name"] = ViewBag.user_name;

                    string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                    ViewBag.user_type = user_type;
                    TempData["user_type"] = ViewBag.user_type;

                    ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                    TempData["user_image"] = ViewBag.user_image;

                    ViewBag.user_email = context.user_master.Where(i => i.user_id == id).Select(u => u.user_email).FirstOrDefault();
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult ProfileView(user_master um, System.Web.HttpPostedFileBase fileUpload)
        {

            if(fileUpload == null)
            {
                return RedirectToAction("ProfileView", "Profile");
            }

            if(fileUpload != null && fileUpload.ContentLength > 0)
            {
                //extract file name
                var fileName = Path.GetFileName(fileUpload.FileName);
                //store the file inside ~/App_Data/uploads folder
                var path = Path.Combine(Server.MapPath("~/uploads/"), fileName);
                fileUpload.SaveAs(path);

                if (ModelState.IsValid)
                {

                    //Update fields
                    using (IPLEntities i = new IPLEntities())
                    {

                        int user_id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);

                        var result = i.user_master.Where(a => a.user_id == user_id).FirstOrDefault();
                        if (result != null)
                        {
                            result.user_image = Url.Content("~/uploads/" + fileName);
                            //result.user_image = new byte[fileUpload.ContentLength];
                            //fileUpload.InputStream.Read(result.user_image, 0, fileUpload.ContentLength);
                            //i.Entry(um).State = EntityState.Modified;
                            i.SaveChanges();
                            return RedirectToAction("ProfileView", "Profile");
                        }
                    }

                }
                
            }

            return RedirectToAction("ProfileView", "Profile");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult UpdatePassword(string txtCurrentPassword, string txtNewPassword, string txtReNewPassword, user_master user_m)
        {
            if (ModelState.IsValid)
            {
                
                if(!(txtNewPassword.Length >= 6))
                {
                    TempData["length_error"] = "Password should be of minimum 6 characters";
                    return RedirectToAction("ProfileView", "Profile");
                }

                if(!(txtNewPassword == txtReNewPassword))
                {
                    TempData["error"] = "Passwords do not match!";
                    return RedirectToAction("ProfileView", "Profile");
                    //return Content("<script language='javascript' type='text/javascript'>alert('Passwords do not match');</script>");
                }

                using (IPLEntities i = new IPLEntities())
                {
                    int user_id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
                    var result = i.user_master.Where(a => a.user_id == user_id).FirstOrDefault();
                    if (result != null)
                    {
                        if (result.user_password != txtCurrentPassword)
                        {
                            TempData["message"] = "Invalid current password entered!";
                            return RedirectToAction("ProfileView", "Profile");
                        }

                        result.user_password = txtNewPassword;
                        i.SaveChanges();
                        TempData["success"] = "Password changed successfully!";
                        return RedirectToAction("Logout", "Home");
                    }


                }
            }
            return RedirectToAction("ProfileView", "Profile");
        }
    }
}