﻿using IPLAuction.Models;
using IPLAuction.Models.Extended;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPLTrial.Controllers
{
    public class SeasonMasterController : Controller
    {
        IPLEntities ipl = new IPLEntities();
        PlayerUtility player_Util = new PlayerUtility();
        TeamUtility team_Util = new TeamUtility();

        // GET: SeasonMaster
        public ActionResult AddSeason()
        {
            ViewBag.orange_cap = player_Util.GetAllActivePlayers();
            ViewBag.purple_cap = player_Util.GetAllActivePlayers();
            ViewBag.man_series = player_Util.GetAllActivePlayers();
            ViewBag.host_country = player_Util.GetCountryListForHost();

            int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
            if (id != 0)
            {
                ViewBag.user_id = id;

                string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                ViewBag.user_name = user_name;
                TempData["user_name"] = ViewBag.user_name;

                string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                ViewBag.user_type = user_type;
                TempData["user_type"] = ViewBag.user_type;

                ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                TempData["user_image"] = ViewBag.user_image;

            }

            return View();
        }

        [HttpPost]
        public ActionResult AddSeason(season_master season)
        {

            ViewBag.orange_cap = player_Util.GetAllActivePlayers();
            ViewBag.purple_cap = player_Util.GetAllActivePlayers();
            ViewBag.man_series = player_Util.GetAllActivePlayers();
            ViewBag.host_country = player_Util.GetCountryListForHost();

            if (ModelState.IsValid)
            {
                var query = ipl.season_master.Where(s => s.season_year == season.season_year).FirstOrDefault();
                if (query != null)
                {
                    ViewBag.Validate = false;
                    return View();
                }

                ipl.season_master.Add(season);
                ipl.SaveChanges();
                ViewBag.Added = true;
                ModelState.Clear();
                return View();
            }
            else
            {
                ViewBag.Success = false;
                return View();
            }

        }

        public ActionResult EditNewSeason()
        {

            ViewBag.players = player_Util.GetAllActivePlayers();
            int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
            if (id != 0)
            {
                ViewBag.user_id = id;

                string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                ViewBag.user_name = user_name;
                TempData["user_name"] = ViewBag.user_name;

                string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                ViewBag.user_type = user_type;
                TempData["user_type"] = ViewBag.user_type;

                ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                TempData["user_image"] = ViewBag.user_image;

            }

            using (IPLEntities ipl = new IPLEntities())
            {
                var season_id = team_Util.GetCurrentSeason();
                var v = ipl.season_master.Where(s => s.season_id == season_id).FirstOrDefault();
                var host_country = ipl.country_master.Where(s => s.country_id == v.country_id).FirstOrDefault();
                TempData["year"] =  v.season_year;
                TempData["host_country"] = host_country.country_name;
                TempData["all_players"] = ViewBag.players;
                return PartialView(v);

            }
          
        }

        [HttpPost]
        public ActionResult EditNewSeason(season_master season, Int16 txtYear, string txtCountry, FormCollection form)
        {

            ViewBag.players = player_Util.GetAllActivePlayers();
            var season_id = team_Util.GetCurrentSeason();
            var country_id = team_Util.GetCurrentSeasonHostCountry();
            var season_year = ipl.season_master.Where(s => s.season_id == season_id).FirstOrDefault();
            if (season_year == null)
            {
                season_year = null;
            }
            var host_country = ipl.country_master.Where(s => s.country_id == country_id).FirstOrDefault();
            TempData["year"] = season_year.season_year; ;
            TempData["host_country"] = host_country.country_name;
            TempData["all_players"] = ViewBag.players;

            if (!ModelState.IsValid )
            {
                ViewBag.Success = false;
                return View();
            }
          

                if (ModelState.IsValid)
                {
                using (IPLEntities ipl = new IPLEntities())
                {
                    if (season_id > 0)
                    {
                        //Edit
                        var v = ipl.season_master.Where(a => a.season_id == season_id).FirstOrDefault();
                        if (v != null)
                        {
                            v.purple_cap_player_id = season.purple_cap_player_id;
                            v.orange_cap_player_id = season.orange_cap_player_id;
                            v.man_series_player_id = season.man_series_player_id;
                            v.sixes_count = season.sixes_count;
                            v.catches_count = season.catches_count;
                        }
                    }
                    else
                    {
                        //Save
                        ipl.season_master.Add(season);
                    }
                    ipl.SaveChanges();
                    ViewBag.Success = true;
                }


                return RedirectToAction("EditNewSeason", "SeasonMaster");
                }
            return View();
            }
        }
        
    }