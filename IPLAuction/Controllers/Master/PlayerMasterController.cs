﻿using IPLAuction.Models;
using IPLAuction.Models.Extended;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IPLTrial.Controllers
{
    public class PlayerMasterController : Controller
    {
        PlayerUtility pu = new PlayerUtility();
        IPLEntities ipl = new IPLEntities();
        TeamUtility teamUtility = new TeamUtility();

        // GET: PlayerMaster
        public ActionResult AddPlayer()
        {

            ViewBag.batting = pu.GetBattingStyle();
            ViewBag.bowling = pu.GetBowlingSkills();
            ViewBag.country = pu.GetCountryList();
            Response.Cache.SetNoStore();

            using (var context = new IPLEntities())
            {

                ViewBag.count_matches = context.match_master.ToList().Count;
                ViewBag.count_players = context.player_master.ToList().Count;
                ViewBag.count_teams = context.team_master.ToList().Count;

                int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
                if (id != 0)
                {
                    ViewBag.user_id = id;

                    string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                    ViewBag.user_name = user_name;
                    TempData["user_name"] = ViewBag.user_name;

                    string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                    ViewBag.user_type = user_type;
                    TempData["user_type"] = ViewBag.user_type;

                    ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                    TempData["user_image"] = ViewBag.user_image;

                }
                else
                {
                    TempData["user_image"] = "/uploads/default-user-image.png";
                }
            }

            return View();
        }


        [HttpPost]
        public ActionResult AddPlayer(player_master player, player_skills skill, System.Web.HttpPostedFileBase fileUpload)

        {
            ViewBag.batting = pu.GetBattingStyle();
            ViewBag.bowling = pu.GetBowlingSkills();
            ViewBag.country = pu.GetCountryList();

            if (fileUpload != null && fileUpload.ContentLength > 0)
            {
                //extract file name
                var fileName = Path.GetFileName(fileUpload.FileName);
                //store the file inside ~/App_Data/uploads folder
                var path = Path.Combine(Server.MapPath("~/uploads/"), fileName);
                fileUpload.SaveAs(path);


                if (ModelState.IsValid)
                {
                    player.selected = false;
                    player.player_image = Url.Content("~/uploads/" + fileName);

                    ipl.player_master.Add(player);


                    /*Base Fare calculation*/
                    var base_price = 50000;
                    var batting_star = skill.player_batting_star;
                    var bowling_star = skill.player_bowling_star;
                    switch (batting_star)
                    {
                        case 1: base_price += 0;
                            break;
                        case 2: base_price += 10000;
                            break;
                        case 3: base_price += 20000;
                            break;
                        case 4: base_price += 30000;
                            break;
                        case 5: base_price += 45000;
                            break;
                    }
                    switch (bowling_star)
                    {
                        case 1:
                            base_price += 7000;
                            break;
                        case 2:
                            base_price += 13000;
                            break;
                        case 3:
                            base_price += 24000;
                            break;
                        case 4:
                            base_price += 38000;
                            break;
                        case 5:
                            base_price += 49000;
                            break;
                    }

                    skill.player_base_price = base_price;
                    skill.player_master = player;

                    ipl.player_skills.Add(skill);
                    ipl.SaveChanges();
                    player.player_image_id = player.player_id;
                    ipl.SaveChanges();

                    ViewBag.Added = true;
                    ViewBag.new_player_name = player.player_name;

                    ModelState.Clear();
                    return View();
                }
                else
                {
                    ViewBag.Success = false;
                }
            }

            return View();
        }

        //get View: Battingskill
        public ActionResult NewBattingSkill()
        {


            Response.Cache.SetNoStore();

            using (var context = new IPLEntities())
            {

                ViewBag.count_matches = context.match_master.ToList().Count;
                ViewBag.count_players = context.player_master.ToList().Count;
                ViewBag.count_teams = context.team_master.ToList().Count;

                int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
                if (id != 0)
                {
                    ViewBag.user_id = id;

                    string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                    ViewBag.user_name = user_name;
                    TempData["user_name"] = ViewBag.user_name;

                    string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                    ViewBag.user_type = user_type;
                    TempData["user_type"] = ViewBag.user_type;

                    ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                    TempData["user_image"] = ViewBag.user_image;

                }
                else
                {
                    TempData["user_image"] = "/uploads/default-user-image.png";
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult NewBattingSkill(bowling_skill_master bowling)
        {

            Response.Cache.SetNoStore();
            if (ModelState.IsValid)
            {

                var query = ipl.bowling_skill_master.Where(a => a.bowling_skill == bowling.bowling_skill).Select(a => a.bowling_skill).FirstOrDefault();
                if (query != null)
                {
                    ViewBag.Success_Bowling_Validate = false;
                }
                else
                {
                    ipl.bowling_skill_master.Add(bowling);
                    ipl.SaveChanges();
                    ViewBag.Added_Bowling = true;

                    ModelState.Clear();
                    return View();
                }
            }
            else
            {
                ViewBag.Success_Bowling = false;
            }

            return View();
        }

        //get View: new country
        public ActionResult NewCountry()
        {

            Response.Cache.SetNoStore();

            using (var context = new IPLEntities())
            {

                ViewBag.count_matches = context.match_master.ToList().Count;
                ViewBag.count_players = context.player_master.ToList().Count;
                ViewBag.count_teams = context.team_master.ToList().Count;

                int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
                if (id != 0)
                {
                    ViewBag.user_id = id;

                    string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                    ViewBag.user_name = user_name;
                    TempData["user_name"] = ViewBag.user_name;

                    string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                    ViewBag.user_type = user_type;
                    TempData["user_type"] = ViewBag.user_type;

                    ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                    TempData["user_image"] = ViewBag.user_image;

                }
                else
                {
                    TempData["user_image"] = "/uploads/default-user-image.png";
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult NewCountry(country_master country)
        {
            Response.Cache.SetNoStore();
            if (ModelState.IsValid)
            {

                var query = ipl.country_master.Where(a => a.country_name == country.country_name).Select(a => a.country_name).FirstOrDefault();
                if (query != null)
                {
                    ViewBag.Success_Country_Validate = false;
                }
                else
                {
                    ipl.country_master.Add(country);
                    ipl.SaveChanges();
                    ViewBag.Added_Country = true;

                    ModelState.Clear();
                    return View();
                }
            }
            else
            {
                ViewBag.Success_Country = false;
            }

            return View();
        }

        //get View: new City
        public ActionResult NewCity()
        {
            Response.Cache.SetNoStore();
            ViewBag.country = pu.GetCountryList();

            using (var context = new IPLEntities())
            {

                ViewBag.count_matches = context.match_master.ToList().Count;
                ViewBag.count_players = context.player_master.ToList().Count;
                ViewBag.count_teams = context.team_master.ToList().Count;

                int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
                if (id != 0)
                {
                    ViewBag.user_id = id;

                    string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                    ViewBag.user_name = user_name;
                    TempData["user_name"] = ViewBag.user_name;

                    string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                    ViewBag.user_type = user_type;
                    TempData["user_type"] = ViewBag.user_type;

                    ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                    TempData["user_image"] = ViewBag.user_image;

                }
                else
                {
                    TempData["user_image"] = "/uploads/default-user-image.png";
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult NewCity(city_master city)
        {
            ViewBag.country = pu.GetCountryList();

            Response.Cache.SetNoStore();
            if (ModelState.IsValid)
            {

                var query = ipl.city_master.Where(a => a.city_name == city.city_name).Select(a => a.city_name).FirstOrDefault();
                if (query != null)
                {
                    ViewBag.Success_City_Validate = false;
                }
                else
                {
                    ipl.city_master.Add(city);
                    ipl.SaveChanges();
                    ViewBag.Added_City = true;

                    ModelState.Clear();
                    return View();
                }
            }
            else
            {
                ViewBag.Success_City = false;
            }

            return View();
        }

        //get View: new Venue
        public ActionResult NewVenue()
        {
            ViewBag.city = teamUtility.GetCity();

            Response.Cache.SetNoStore();

            using (var context = new IPLEntities())
            {

                ViewBag.count_matches = context.match_master.ToList().Count;
                ViewBag.count_players = context.player_master.ToList().Count;
                ViewBag.count_teams = context.team_master.ToList().Count;

                int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
                if (id != 0)
                {
                    ViewBag.user_id = id;

                    string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                    ViewBag.user_name = user_name;
                    TempData["user_name"] = ViewBag.user_name;

                    string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                    ViewBag.user_type = user_type;
                    TempData["user_type"] = ViewBag.user_type;

                    ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                    TempData["user_image"] = ViewBag.user_image;

                }
                else
                {
                    TempData["user_image"] = "/uploads/default-user-image.png";
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult NewVenue(venue_master venue)
        {
            ViewBag.city = teamUtility.GetCity();

            Response.Cache.SetNoStore();
            if (ModelState.IsValid)
            {

                var query = ipl.venue_master.Where(a => a.venue_name== venue.venue_name).Select(a => a.venue_name).FirstOrDefault();
                if (query != null)
                {
                    ViewBag.Success_Venue_Validate = false;
                }
                else
                {
                    ipl.venue_master.Add(venue);
                    ipl.SaveChanges();
                    ViewBag.Added_Venue = true;

                    ModelState.Clear();
                    return View();
                }
            }
            else
            {
                ViewBag.Success_Venue = false;
            }

            return View();
        }
    }
    public interface HttpPostedFileBase
    {
        int Length { get; }
        object ContentDisposition { get; }
        string FileName { get; }

        Stream OpenReadStream();
        void SaveAs(string physicalPath);
        Task SaveAsAsync(string v);
    }

    
}