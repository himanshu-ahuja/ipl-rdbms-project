﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IPLAuction.Models;
using System;
using IPLAuction.Models.Extended;
using System.IO;
using System.Threading.Tasks;

namespace IPLTrial.Controllers
{
    public class NewTeamController : Controller
    {

        IPLEntities ipl = new IPLEntities();
        TeamUtility util = new TeamUtility();

        public ActionResult AddNewTeam()
        {
            //Populating cities in dropdown 
            ViewBag.cities = util.GetCityHostWise();

            //Populating owners in dropdown
            ViewBag.owners = util.GetOwner();

            ViewBag.coaches = util.GetCoaches();

            Response.Cache.SetNoStore();

            using (var context = new IPLEntities())
            {

                ViewBag.count_matches = context.match_master.ToList().Count;
                ViewBag.count_players = context.player_master.ToList().Count;
                ViewBag.count_teams = context.team_master.ToList().Count;

                int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
                if (id != 0)
                {
                    ViewBag.user_id = id;

                    string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                    ViewBag.user_name = user_name;
                    TempData["user_name"] = ViewBag.user_name;

                    string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                    ViewBag.user_type = user_type;
                    TempData["user_type"] = ViewBag.user_type;

                    ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                    TempData["user_image"] = ViewBag.user_image;

                }
            }

            return View();
        }

        public JsonResult getVenue(int id)
        {
           List<SelectListItem> venue =  util.GetVenueList(id);
            return Json(new SelectList(venue, "Value", "Text", JsonRequestBehavior.AllowGet));
        }
        
        [HttpPost]
        public ActionResult AddNewTeam(team_master team, FormCollection form, System.Web.HttpPostedFileBase fileUpload, string txtBudget, team_budget team_budget)
        {
            string budget = "0";

            if (!ModelState.IsValid || fileUpload == null)
            {
                ViewBag.Success = false;
                ViewBag.cities = util.GetCity();
                ViewBag.owners = util.GetOwner();
                ViewBag.coaches = util.GetCoaches();
                return View();
            }
            ViewBag.cities = util.GetCity();
            ViewBag.owners = util.GetOwner();
            ViewBag.coaches = util.GetCoaches();

            string id = form["venue"].ToString();

            if (id == null || id == "--Select Homeground--")
            {
                ViewBag.Success = false;
                ViewBag.cities = util.GetCity();
                ViewBag.owners = util.GetOwner();
                ViewBag.coaches = util.GetCoaches();
                return View();
            }
            team.homeground = Int32.Parse(id);

            if (fileUpload != null && fileUpload.ContentLength > 0)
            {
                //extract file name
                var fileName = Path.GetFileName(fileUpload.FileName);
                //store the file inside ~/App_Data/uploads folder
                var path = Path.Combine(Server.MapPath("~/uploads/"), fileName);
                fileUpload.SaveAs(path);

                

                if (ModelState.IsValid)
                {
                    if (ipl.team_master.Any(a => a.team_name == team.team_name))
                    {
                        ViewBag.Validate = false;
                        return View();
                    }

                    team.team_logo = Url.Content("~/uploads/" + fileName);
                    TempData["team_logo"] = team.team_logo;
                    TempData.Keep("team_logo");
                    budget = Request.Form["txtBudget"];

                    team.team_founded = DateTime.Now;
                    ipl.team_master.Add(team);

                    ipl.SaveChanges();

                    int team_id = team.team_id;
                    string team_name = team.team_name;

                    var season = ipl.season_master.Max(s => s.season_year);
                    var season_id = ipl.season_master.Where(a => a.season_year == season).FirstOrDefault();
                    team_budget.season_id = season_id.season_id;
                    team_budget.team_id = team_id;
                    team_budget.total_budget = Convert.ToDecimal(budget);
                    ipl.team_budget.Add(team_budget);
                    ipl.SaveChanges();

                    TempData["team_id"] = team_id;
                    TempData.Keep("team_id");
                    TempData["team_name"] = team_name;
                
                    TempData["team_success"] = true;
                    TempData.Keep("team_succcess");
                   
                    return RedirectToAction("TeamSquad", "TeamSquad");
                }
                
            }
            return View();
        }

        public interface HttpPostedFileBase
        {
            int Length { get; }
            object ContentDisposition { get; }
            string FileName { get; }

            Stream OpenReadStream();
            void SaveAs(string physicalPath);
            Task SaveAsAsync(string v);
        }

    }
}
    