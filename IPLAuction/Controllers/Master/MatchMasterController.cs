﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IPLAuction.Models;
using System;
using IPLAuction.Models.Extended;

namespace IPLTrial.Controllers
{
    public class MatchMasterController : Controller
    {

        IPLEntities ipl = new IPLEntities();
        MatchUtility matchUtil = new MatchUtility();
        PlayerUtility playerUtil = new PlayerUtility();
        TeamUtility TeamUtil = new TeamUtility();
        // GET: MatchMaster
        public ActionResult EditMatch()
        {

            ViewBag.team = matchUtil.GetActiveTeam();
            ViewBag.umpire = matchUtil.GetUmpire();
            ViewBag.cities = TeamUtil.GetCityHostWise();
            ViewBag.coaches = TeamUtil.GetCoaches();
            
            return View();
        }

        public ActionResult GetMatches()
        {
            ViewBag.team = matchUtil.GetActiveTeam();
            ViewBag.umpire = matchUtil.GetUmpire();
            ViewBag.cities = TeamUtil.GetCityHostWise();
            ViewBag.coaches = TeamUtil.GetCoaches();

            DateTime dateTime = DateTime.UtcNow.Date;

            var match = new
            {
                data =

                        (from m in ipl.match_master

                         join c in ipl.city_master on m.city_id equals c.city_id
                         join v in ipl.venue_master on m.city_id equals v.city_id
                         join t in ipl.team_master on m.team_id equals t.team_id
                         join t1 in ipl.team_master on m.opponent_team_id equals t1.team_id
                         join toss in ipl.team_master on m.toss_winner_team_id equals toss.team_id into gToss
                         join winner in ipl.team_master on m.match_winner_team_id equals winner.team_id into gWinner
                         join player in ipl.player_master on m.man_match_player_id equals player.player_id into gPlayer
                         join umpire1 in ipl.umpire_master on m.first_umpire_id equals umpire1.umpire_id into gUmpire1
                         join umpire2 in ipl.umpire_master on m.second_umpire_id equals umpire2.umpire_id into gUmpire2


                         from toss in gToss.DefaultIfEmpty()
                         from winner in gWinner.DefaultIfEmpty()
                         from player in gPlayer.DefaultIfEmpty()
                         from umpire1 in gUmpire1.DefaultIfEmpty()
                         from umpire2 in gUmpire2.DefaultIfEmpty()

                         where m.match_date >= dateTime

                         select new
                         {
                             m.match_date,
                             c.city_name,
                             v.venue_name,
                             team = t.team_id.ToString() == null ? String.Empty : t.team_name,
                             opponent = t1.team_id.ToString() == null ? String.Empty : t1.team_name,
                             toss_winner = toss.team_id.ToString() == null ? String.Empty : toss.team_name,
                             toss_decision = m.toss_decision,
                             //Is_superover = m.IS_Superover,
                             Is_result = m.IS_Result,
                             win_type = m.win_type,
                             won_by = m.won_by,
                             match_winner = winner.team_id.ToString() == null ? String.Empty : winner.team_name,
                             man_of_the_match = player.player_id.ToString() == null ? String.Empty : player.player_name,
                             first_umpire = umpire1.umpire_id.ToString() == null ? String.Empty : umpire1.umpire_name,
                             second_umpire = umpire2.umpire_id.ToString() == null ? String.Empty : umpire2.umpire_name,
                             m.match_id
                         }).ToList()
            };

            return Json(match, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult EditMatchInfo(int id)
        {
            ViewBag.team = matchUtil.GetActiveTeam();
            ViewBag.umpire = matchUtil.GetUmpire();
            ViewBag.cities = TeamUtil.GetCityHostWise();
            ViewBag.coaches = TeamUtil.GetCoaches();

            using (IPLEntities ipl = new IPLEntities())
            {
                var v = ipl.match_master.Where(a => a.match_id == id).FirstOrDefault();
               
                var city = ipl.city_master.Where(c => c.city_id == v.city_id).FirstOrDefault();
                TempData["city"] = city.city_name;
                var venue = ipl.venue_master.Where(c => c.venue_id == v.venue_id).FirstOrDefault();
                TempData["venue"] = venue.venue_name;
                var team = ipl.team_master.Where(c => c.team_id == v.team_id).FirstOrDefault();
                TempData["team"] = team.team_name;
                var opponent = ipl.team_master.Where(c => c.team_id == v.opponent_team_id).FirstOrDefault();
                TempData["opponent_team"] = team.team_name;
                TempData["umpire"] = ViewBag.umpire;
                return PartialView(v);
            }
        }

        [HttpPost]
        public ActionResult EditMatchInfo(match_master match,string toss_winner, string match_winner, string man_of_the_match, string umpire2, FormCollection form)
        {
            ViewBag.team = matchUtil.GetActiveTeam();
            ViewBag.umpire = matchUtil.GetUmpire();
            ViewBag.cities = TeamUtil.GetCityHostWise();
            ViewBag.coaches = TeamUtil.GetCoaches();

            bool status = false;
            if (ModelState.IsValid)
            {
                using (IPLEntities ipl = new IPLEntities())
                {
                    if (match.match_id > 0)
                    {
                        //Edit
                        var v = ipl.match_master.Where(a => a.match_id == match.match_id).FirstOrDefault();
                        if (v != null)
                        {
                            v.toss_winner_team_id = Convert.ToInt32(toss_winner);
                            v.match_winner_team_id = Convert.ToInt32(match_winner);
                            v.IS_Result = match.IS_Result;
                            v.won_by = match.won_by;
                            v.toss_decision = match.toss_decision;
                            v.IS_Superover = false;
                            //v.IS_Superover = match.IS_Superover;
                            v.win_type = match.win_type;
                            v.man_match_player_id = Convert.ToInt32(man_of_the_match);
                            v.first_umpire_id = match.first_umpire_id;
                            v.second_umpire_id = Convert.ToInt32(umpire2);
                        }
                    }
                    else
                    {
                        //Save
                        ipl.match_master.Add(match);
                    }
                    ipl.SaveChanges();
                    status = true;
                    return RedirectToAction("EditMatch", "MatchMaster");
                }
            }
            return RedirectToAction("EditMatch", "MatchMaster");
        }


        //GET: New Match View
        public ActionResult NewMatch()
        {
            ViewBag.team = matchUtil.GetActiveTeam();
            ViewBag.cities = TeamUtil.GetCityHostWise();
            return View();
        }

        [HttpPost]
        public ActionResult NewMatch(match_master match, int opponent, int venue)
        {
            ViewBag.team = matchUtil.GetActiveTeam();
            ViewBag.cities = TeamUtil.GetCity();

            if (ModelState.IsValid)

            {

                var check = ipl.match_master.Where(a => a.match_date == match.match_date && a.team_id == match.team_id && a.opponent_team_id == match.opponent_team_id).FirstOrDefault();
                    if(check != null)
                {
                    ViewBag.Added = false;
                    return View();
                }

                var dateCheck = ipl.match_master.Where(a => a.match_date == match.match_date && a.team_id == match.team_id).FirstOrDefault();
                if (dateCheck != null)
                {
                    ViewBag.Added = false;
                    return View();
                }

                match.season_id = TeamUtil.GetCurrentSeason();
                match.country_id = TeamUtil.GetCurrentSeasonHostCountry();
                match.opponent_team_id = opponent;
                match.venue_id = venue;
                ipl.match_master.Add(match);
                ipl.SaveChanges();

                var team1 = ipl.counters.Where(t => t.team_id == match.team_id).FirstOrDefault();
                var team2 = ipl.counters.Where(t => t.team_id == match.opponent_team_id).FirstOrDefault();
                int team1_count = team1.matches_played;
                int team2_count = team2.matches_played;
                team1_count += 1;
                team2_count += 1;

                team1.matches_played = team1_count;
                team2.matches_played = team2_count;
                ipl.SaveChanges();

                ViewBag.Added = true;
                ModelState.Clear();
                return View();
            }
            else
            {
                ViewBag.Success = false;
                return View();
            }

        }

        public JsonResult getOpponent(int id)
        {
            List<SelectListItem> opponent = matchUtil.GetOpponent(id);
            return Json(new SelectList(opponent, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        public JsonResult getTossWinner(int id, int oppo)
        {
            List<SelectListItem> toss = matchUtil.GetTossWinner(id, oppo);
            return Json(new SelectList(toss, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        public JsonResult getVenue(int id)
        {
            List<SelectListItem> venues = TeamUtil.GetVenueList(id);
            return Json(new SelectList(venues, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        public JsonResult getPlayers(int id)
        {
            List<SelectListItem> players = TeamUtil.GetActiveTeamPlayers(id);
            return Json(new SelectList(players, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        public JsonResult getUmpire(int id)
        {
            List<SelectListItem> umpire2 = matchUtil.GetSecondUmpire(id);
            return Json(new SelectList(umpire2, "Value", "Text", JsonRequestBehavior.AllowGet));
        }


    }
}