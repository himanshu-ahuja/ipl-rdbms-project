﻿using IPLAuction.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPLTrial.Controllers
{
    public class NewOwnerController : Controller
    {
        IPLEntities ipl = new IPLEntities();
        IPLAuction.Models.Extended.urlContent url = new IPLAuction.Models.Extended.urlContent();

        public ActionResult AddOwner()
        {
           
            return View();
        }

        [HttpPost]
       // [ValidateAntiForgeryToken]
        public ActionResult AddOwner(user_master user, System.Web.HttpPostedFileBase fileUpload)
        {
            if (!ModelState.IsValid || fileUpload == null)
            {
                ViewBag.Success = false;
               
                return View();
            }

            if (fileUpload != null && fileUpload.ContentLength > 0)
            {
                //extract file name
                var fileName = Path.GetFileName(fileUpload.FileName);
                //store the file inside ~/App_Data/uploads folder
                var path = Path.Combine(Server.MapPath("~/uploads/"), fileName);
                fileUpload.SaveAs(path);
             
                if (ModelState.IsValid)
                {
                    if (ipl.user_master.Any(a => a.user_email == user.user_email))
                    {
                        ViewBag.Validate = false;
                    
                        return View();
                    }
                    user.user_image = Url.Content("~/uploads/" + fileName);
                    ipl.user_master.Add(user);
                    ipl.SaveChanges();
                    ViewBag.Added = true;
                    ModelState.Clear();
                    url.urlDamnFix();
                    return View();
                }
                else
                {
                    ViewBag.Added = false;
                }
            }
     
            return View();
        }



    }
}