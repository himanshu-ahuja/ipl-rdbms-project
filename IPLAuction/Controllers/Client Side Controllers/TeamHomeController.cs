﻿using IPLAuction.Models;
using IPLAuction.Models.Extended;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace IPLAuction.Controllers.Client_Side_Controllers
{
    public class TeamHomeController : Controller
    {
        IPLEntities ipl = new IPLEntities();
        TeamUtility teamUtil = new TeamUtility();
        MatchUtility matchUtil = new MatchUtility();
        PlayerUtility playerUtil = new PlayerUtility();
      
        // GET: TeamHome
        public ActionResult Team()
        {
            int u_id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
            //print team name
            var team = ipl.team_master.Where(u => u.owner_id == u_id).FirstOrDefault();
            TempData["team_name"] = team.team_name;
            var team_id = team.team_id;
            TempData["team_id"] = team_id;
            var city = ipl.city_master.Where(u => u.city_id == team.homecity).FirstOrDefault();
            var venue = ipl.venue_master.Where(u => u.venue_id == team.homeground).FirstOrDefault();
            var about = ipl.team_about.Where(u => u.team_id == team.team_id).FirstOrDefault();

            TempData["home_city"] = city.city_name;
            TempData["home_ground"] = venue.venue_name;
            TempData["about_team"] = about.about;
            char[] home = (venue.venue_name.ToCharArray().Where(c => !Char.IsWhiteSpace(c)).ToArray());
            string home1 = home.ToString();
        
            TempData["ground"] = home1;
            TempData.Keep("team_name");
            TempData.Keep("home_city");
            TempData.Keep("home_ground");
            TempData.Keep("about_team");

            //all seasons dropdown
            var season_list = ipl.season_master.ToList();
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var i in season_list)
            {
                if (i.season_year == 2016)
                {
                    items.Add(new SelectListItem { Text = i.season_year.ToString(), Value = i.season_id.ToString(), Selected = true });
                }
                else
                {
                    items.Add(new SelectListItem { Text = i.season_year.ToString(), Value = i.season_id.ToString() });
                }
            }
            ViewBag.season_list = items;


            //fetching team budget
            var season_id = teamUtil.GetCurrentSeason();
            var budget = ipl.team_budget.Where(a => a.team_id == team.team_id).FirstOrDefault();
            var season_budget = ipl.team_budget.Where(a => a.season_id == season_id && a.team_id == budget.team_id).FirstOrDefault();
            var total_budget = season_budget.total_budget;
            var budget_spent = season_budget.budget_spent;
            var current_season_year = ipl.season_master.Where(s => s.season_id == season_id).Select(s => s.season_year).FirstOrDefault();
            TempData["current_season_year"] = current_season_year;
            TempData["total_budget"] = total_budget;
            TempData["budget_spent"] = budget_spent;


            //previous year budget
            var prev_bud_id = (season_budget.season_id - 1);
                if(prev_bud_id == 0)
            {
                prev_bud_id = season_budget.budget_id;
            }

            var previous_budget = ipl.team_budget.Where(a => a.season_id == prev_bud_id && a.team_id == budget.team_id).FirstOrDefault();
                if(previous_budget == null)
            {
                return View();
            }
            var prev_total_budget = previous_budget.total_budget;
            var prev_budget_spent = previous_budget.budget_spent;
            TempData["prev_total_budget"] = prev_total_budget;
            TempData["prev_budget_spent"] = prev_budget_spent;

            return View();
        }

        public ActionResult GetSquad(int id)
        {
            int u_id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);

            if (id == 0)
            {
                id = teamUtil.GetCurrentSeason();
            }

            ipl.Configuration.ProxyCreationEnabled = false;

            var players = new
            {
                data =

                    (from owner in ipl.user_master
                     join team in ipl.team_master on owner.user_id equals team.owner_id
                     join squad in ipl.team_squad on team.team_id equals squad.team_id
                     join player in ipl.player_master on squad.player_id equals player.player_id
                     join bat in ipl.batting_style_master on player.player_batting_style equals bat.batting_id
                     join bowl in ipl.bowling_skill_master on player.player_bowling_skill equals bowl.bowling_skill_id
                     join country in ipl.country_master on player.country equals country.country_id
                     join skills in ipl.player_skills on player.player_id equals skills.player_id into skill_join
                     from skills in skill_join.DefaultIfEmpty()

                     where owner.user_id == u_id && squad.season_id == id

                     orderby squad.season_id, player.player_id

                     let batting_star = ipl.player_skills.Where(s => s.player_id == player.player_id)
                     let batting = batting_star.Any() ? batting_star.Sum(x => x.player_batting_star) : 0

                     let bowling_star = ipl.player_skills.Where(s => s.player_id == player.player_id)
                     let bowling = bowling_star.Any() ? bowling_star.Sum(x => x.player_bowling_star) : 0

                     select new
                     {

                         player.player_name,
                         player.player_dob,
                         batting = bat.batting_id.ToString() == null ? String.Empty : bat.batting_style,
                         bat_star = batting,
                         bowling = bowl.bowling_skill_id.ToString() == null ? String.Empty : bowl.bowling_skill,
                         bowl_star = bowling,
                         country = player.country.ToString() == null ? String.Empty : country.country_name


                     }).ToList()
            };
            return Json(players, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetSchedule()
        {
            DateTime dateTime = DateTime.UtcNow.Date;

            int u_id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
            var team = ipl.team_master.Where(u => u.owner_id == u_id).FirstOrDefault();

            var match = new
            {
                data =

                             (from m in ipl.match_master.DefaultIfEmpty()

                              join c in ipl.city_master on m.city_id equals c.city_id
                              join v in ipl.venue_master on m.city_id equals v.city_id
                              join t in ipl.team_master on m.team_id equals t.team_id
                              join t1 in ipl.team_master on m.opponent_team_id equals t1.team_id

                              where t.team_id == team.team_id && m.match_date >= dateTime
                              //orderby m.match_date
                              select new
                              {
                                  match_date = m.match_date,
                                  city_name = c.city_name,
                                  venue_name = v.venue_name,
                                  logo = t.team_logo,
                                  team = t.team_id.ToString() == null ? String.Empty : t.team_name,
                                  opponent = t1.team_id.ToString() == null ? String.Empty : t1.team_name,
                              }).ToList()

            };

            return Json(match, JsonRequestBehavior.AllowGet);
        }
    }
}