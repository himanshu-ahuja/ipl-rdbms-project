﻿using IPLAuction.Models;
using IPLAuction.Models.Extended;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPLTrial.Controllers.Content_View_Controllers
{
    public class PlayerSkillsController : Controller
    {
        IPLEntities ipl = new IPLEntities();
        PlayerUtility play_util = new PlayerUtility();

        // GET: PlayerSkills


        public ActionResult ViewPlayerSkills()
        {
            return View();
        }

        public ActionResult GetPlayers()
        {
            ipl.Configuration.ProxyCreationEnabled = false;

            var player = new
            {
                data =

                 (from p in ipl.player_skills.DefaultIfEmpty()
                  join p1 in ipl.player_master on p.player_id equals p1.player_id
                  join bat in ipl.batting_style_master on p1.player_batting_style equals bat.batting_id
                  join bowl in ipl.bowling_skill_master on p1.player_bowling_skill equals bowl.bowling_skill_id
                  select new
                  {
                      p1.player_image,
                      p1.player_name,
                      batting_id = bat.batting_id.ToString() == null ? String.Empty : bat.batting_style,
                      batting_star = p.player_skill_id.ToString() == null ? 0 : p.player_batting_star,
                      bowling_id = bowl.bowling_skill_id.ToString() == null ? String.Empty : bowl.bowling_skill,
                      bowling_star = p.player_skill_id.ToString() == null ? 0 : p.player_bowling_star,
                      p.player_base_price,
                      p.player_skill_id

                  }).ToList()
            };
            return Json(player, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Save(int id)
        {
            using (IPLEntities ipl = new IPLEntities())
            {
                var v = ipl.player_skills.Where(a => a.player_skill_id == id).FirstOrDefault();
                var bat = ipl.batting_style_master.Where(a => a.batting_id == v.player_master.player_batting_style).FirstOrDefault();
                var bowl = ipl.bowling_skill_master.Where(a => a.bowling_skill_id == v.player_master.player_bowling_skill).FirstOrDefault();

                TempData["skills_player_name"] = v.player_master.player_name;
                TempData["skills_batting_style"] = bat.batting_style;
                TempData["skills_bowling_style"] = bowl.bowling_skill;
                return PartialView(v);

            }
        }

        [HttpPost]
        public ActionResult Save(player_skills skills)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (IPLEntities ipl = new IPLEntities())
                {
                    if (skills.player_skill_id > 0)
                    {
                        //Edit
                        var v = ipl.player_skills.Where(a => a.player_skill_id == skills.player_skill_id).FirstOrDefault();
                        if (v != null)
                        {
                            v.player_batting_star = skills.player_batting_star;
                            v.player_bowling_star = skills.player_bowling_star;
                            v.player_base_price = skills.player_base_price;
                        }
                    }
                    else
                    {
                        //Save
                        ipl.player_skills.Add(skills);
                    }
                    ipl.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }
    }
}