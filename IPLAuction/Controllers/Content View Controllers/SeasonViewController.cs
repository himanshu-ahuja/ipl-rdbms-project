﻿using IPLAuction.Models;
using IPLAuction.Models.Extended;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPLTrial.Controllers
{
    public class SeasonViewController : Controller
    {

        IPLEntities ipl = new IPLEntities();
        PlayerUtility play_util = new PlayerUtility();


        // GET: SeasonView
        public ActionResult SeasonView()
        {
            Response.Cache.SetNoStore();

            using (var context = new IPLEntities())
            {

                ViewBag.count_matches = context.match_master.ToList().Count;
                ViewBag.count_players = context.player_master.ToList().Count;
                ViewBag.count_teams = context.team_master.ToList().Count;

                int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
                if (id != 0)
                {
                    ViewBag.user_id = id;

                    string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                    ViewBag.user_name = user_name;
                    TempData["user_name"] = ViewBag.user_name;

                    string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                    ViewBag.user_type = user_type;
                    TempData["user_type"] = ViewBag.user_type;

                    ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                    TempData["user_image"] = ViewBag.user_image;

                }
            }

            return View();
        }


        public ActionResult GetSeasons()
        {

            //ipl.Configuration.ProxyCreationEnabled = false;

            var season = new
            {
                data =

                (from s in ipl.season_master.DefaultIfEmpty()

                join p in ipl.player_master on s.orange_cap_player_id equals p.player_id into pMap
                join p1 in ipl.player_master on s.purple_cap_player_id equals p1.player_id into pMap1
                 join p2 in ipl.player_master on s.man_series_player_id equals p2.player_id  into pMap2

                 from p in pMap.DefaultIfEmpty()
                 from p1 in pMap1.DefaultIfEmpty()
                 from p2 in pMap2.DefaultIfEmpty()

                 where s.season_year != 0
                 select new
                {
                    s.season_id,
                    s.season_year,
                    orange = p.player_id.ToString() == null ? String.Empty : p.player_name,
                    purple = p1.player_id.ToString() == null ? String.Empty : p1.player_name,
                    man = p2.player_id.ToString() == null ? String.Empty : p2.player_name,
                    s.sixes_count,
                    s.catches_count
                }).ToList()
            };
            return Json(season, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Save(int id)
        {
            ViewBag.players = play_util.GetAllActivePlayers();
            using (IPLEntities ipl = new IPLEntities())
            { 

                var v = ipl.season_master.Where(s => s.season_id == id).FirstOrDefault();
                TempData["players"] = ViewBag.players;
                return PartialView(v);
            }
        }

        [HttpPost]
        public ActionResult Save(season_master season)
        {
            bool status = false;
            

            if(season.man_series_player_id == null)
            {
                return new JsonResult { Data = new { status = status } };
            }
            if(season.orange_cap_player_id == null)
            {
                return new JsonResult { Data = new { status = status } };
            }
            if(season.purple_cap_player_id == null)
            {
                return new JsonResult { Data = new { status = status } };
            }

            if(season.catches_count <= 300 && season.sixes_count <= 300)
            {
                return new JsonResult { Data = new { status = status } };
            }

            if (ModelState.IsValid)
            {
                using (IPLEntities ipl = new IPLEntities())
                {
                    if (season.season_id > 0)
                    {
                        //Edit
                        var v = ipl.season_master.Where(a => a.season_id == season.season_id).FirstOrDefault();
                        if (v != null)
                        {
                            v.man_series_player_id = season.man_series_player_id;
                            v.orange_cap_player_id = season.orange_cap_player_id;
                            v.purple_cap_player_id = season.purple_cap_player_id;
                            v.season_year = season.season_year;
                            v.sixes_count = season.sixes_count;
                            v.catches_count = season.catches_count;
                        }
                    }
                    else
                    {
                        //Save
                        ipl.season_master.Add(season);
                    }
                    ipl.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }

        public ActionResult Delete(int id)
        {
            using(IPLEntities ipl = new IPLEntities())
            {
                var v = ipl.season_master.Where(a => a.season_id == id).FirstOrDefault();
                if (v != null)
                {
                    //return View(v);
                    return PartialView(v);
                }
                else
                {
                    return HttpNotFound();
                }
            }
        }

        [HttpPost]
        //[ActionName("Delete")]
        public ActionResult Delete(int id, season_master season)
        {
            bool status = false;
            using (IPLEntities ipl = new IPLEntities())
            {
                var v = ipl.season_master.Where(a => a.season_id == id).FirstOrDefault();
                if(v!= null)
                {
                    ipl.season_master.Remove(v);
                    ipl.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }
    }
}