﻿using IPLAuction.Models;
using IPLAuction.Models.Extended;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPLTrial.Controllers
{
    public class TeamViewController : Controller
    {

        IPLEntities ipl = new IPLEntities();
        TeamUtility team_util = new TeamUtility();

        // GET: TeamView
        public ActionResult TeamView()
        {
            return View();
        }

        public ActionResult GetActiveTeams()
        {

            ipl.Configuration.ProxyCreationEnabled = false;

            //loading datatable
            var active_teams = new
            {

                data =

               (from t in ipl.team_master
                join o in ipl.user_master on t.owner_id equals o.user_id into owner
                from o in owner.DefaultIfEmpty()
                join c in ipl.city_master on t.homecity equals c.city_id into city
                from c in city.DefaultIfEmpty()
                join v in ipl.venue_master on t.homeground equals v.venue_id into venue
                from v in venue.DefaultIfEmpty()
                join p in ipl.player_master on t.team_captain_player_id equals p.player_id into player
                from p in player.DefaultIfEmpty()
                join co in ipl.coach_master on t.coach_id equals co.coach_id into coach
                from co in coach.DefaultIfEmpty()
               // join match_count in ipl.counters on t.team_id equals match_count.team_id into count
              //  from match_count in count.DefaultIfEmpty()

                select new
                {
                    t.team_id,
                    t.team_name,
                    t.team_short_code,
                    t.team_founded,
                    coach_id = co.coach_id.ToString() == null ? String.Empty : co.coach_name,
                    player_id = p.player_id.ToString() == null ? String.Empty : p.player_name,
                    owner_id = o.user_id.ToString() == null ? String.Empty : o.user_name,
                    city_id = c.city_id.ToString() == null ? String.Empty : c.city_name,
                    venue_id = v.venue_id.ToString() == null ? String.Empty : v.venue_name,
                   // count = match_count.team_id.ToString() == null ? 0 : match_count.matches_played
                }).ToList()
            };

            return Json(active_teams, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Save(int id)
        {
            using (IPLEntities ipl = new IPLEntities())
            {
                var v = ipl.team_master.Where(a => a.team_id == id).FirstOrDefault();
                var coach = ipl.coach_master.Where(a => a.coach_id == v.coach_id).FirstOrDefault();
                var captain = ipl.player_master.Where(a => a.player_id == v.team_captain_player_id).FirstOrDefault();
                var owner = ipl.user_master.Where(a => a.user_id == v.owner_id).FirstOrDefault();
                var homecity = ipl.city_master.Where(a => a.city_id == v.homecity).FirstOrDefault();
                var homeground = ipl.venue_master.Where(a => a.venue_id == v.homeground).FirstOrDefault();

                TempData["team_coach"] = team_util.GetCoaches();
                TempData["team_captain"] = team_util.GetSquad(v.team_id);

                TempData["team_owner"] = owner.user_name;
                TempData["team_homecity"] = homecity.city_name;
                TempData["team_homeground"] = homeground.venue_name;

                return PartialView(v);

            }
        }

        [HttpPost]
        public ActionResult Save(team_master team)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (IPLEntities ipl = new IPLEntities())
                {
                    if (team.team_id > 0)
                    {
                        //Edit
                        var v = ipl.team_master.Where(a => a.team_id == team.team_id).FirstOrDefault();
                        if (v != null)
                        {
                            v.coach_id = team.coach_id;
                            v.team_captain_player_id = team.team_captain_player_id;
                        }
                    }
                    else
                    {
                        //Save
                        ipl.team_master.Add(team);
                    }
                    ipl.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }

    }
}