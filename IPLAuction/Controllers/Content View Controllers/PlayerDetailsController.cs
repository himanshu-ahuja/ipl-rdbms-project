﻿using IPLAuction.Models;
using IPLAuction.Models.Extended;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPLTrial.Controllers.Content_View_Controllers
{
    public class PlayerDetailsController : Controller
    {

        PlayerUtility pu = new PlayerUtility();
        IPLEntities ipl = new IPLEntities();

        // GET: PlayerDetails
        public ActionResult ViewPlayer()
        {
            Response.Cache.SetNoStore();

            using (var context = new IPLEntities())
            {

                ViewBag.count_matches = context.match_master.ToList().Count;
                ViewBag.count_players = context.player_master.ToList().Count;
                ViewBag.count_teams = context.team_master.ToList().Count;

                int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["userID"]);
                if (id != 0)
                {
                    ViewBag.user_id = id;

                    string user_name = System.Web.HttpContext.Current.Session["user_name"].ToString();
                    ViewBag.user_name = user_name;
                    TempData["user_name"] = ViewBag.user_name;

                    string user_type = System.Web.HttpContext.Current.Session["user_type"].ToString();
                    ViewBag.user_type = user_type;
                    TempData["user_type"] = ViewBag.user_type;

                    ViewBag.user_image = System.Web.HttpContext.Current.Session["user_image"];
                    TempData["user_image"] = ViewBag.user_image;

                }
            }

            return View();
        }

        public ActionResult GetPlayers()
        {

            ipl.Configuration.ProxyCreationEnabled = false;

            var player = new
            {
                data =

                 (from p in ipl.player_master
                    
                  join c in ipl.country_master on p.country equals c.country_id
                  join bat in ipl.batting_style_master on p.player_batting_style equals bat.batting_id
                  join bowl in ipl.bowling_skill_master on p.player_bowling_skill equals bowl.bowling_skill_id
                  
                //  where p.active == true
                  select new
                  {
                      p.player_image, 
                      p.player_name,
                      p.player_dob,
                      batting_id = bat.batting_id.ToString() == null ? String.Empty : bat.batting_style,
                      bowling_id = bowl.bowling_skill_id.ToString() == null ? String.Empty : bowl.bowling_skill,
                      c.country_name,
                      p.active,
                      p.player_id
                  }).ToList()
            };
            return Json(player, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PlayerUpdate(int pID, bool status)
        {
            int id = pID;
            bool s = status;
            //bool s = Convert.ToBoolean(status);
            using (IPLEntities i = new IPLEntities())
            {

                var result = i.player_master.Where(a => a.player_id == id).FirstOrDefault();
                if (result != null)
                {
                    if (s != true)
                    {
                        result.active = false;
                    }
                    else
                    {
                        result.active = true;
                    }

                    i.SaveChanges();

                }

                return RedirectToAction("ViewPlayer", "PlayerDetails");
            }

        }

        //public FileContentResult imageGenerate(int id)
        //{
        //    var player_ = ipl.player_master.Find(id);
        //    if (player_ != null)
        //    {
        //        return new FileContentResult(player_.player_image, "image/png");
        //    }
        //    return null;
        //}
    }
}