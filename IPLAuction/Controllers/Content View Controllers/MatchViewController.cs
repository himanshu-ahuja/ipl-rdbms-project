﻿using IPLAuction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPLTrial.Controllers
{
    public class MatchViewController : Controller
    {

        IPLEntities ipl = new IPLEntities();

        // GET: MatchView
        public ActionResult MatchView()

        {
            return View();
        }

        public ActionResult GetMatches()
        {

            ipl.Configuration.ProxyCreationEnabled = false;

            var matches = new
            {
                data =

                  (from m in ipl.match_master

                   join c in ipl.city_master on m.city_id equals c.city_id
                   join v in ipl.venue_master on m.city_id equals v.city_id
                   join t in ipl.team_master on m.team_id equals t.team_id
                   join t1 in ipl.team_master on m.opponent_team_id equals t1.team_id
                   join t2 in ipl.team_master on m.toss_winner_team_id equals t2.team_id
                   join t3 in ipl.team_master on m.match_winner_team_id equals t3.team_id
                   join u in ipl.umpire_master on m.first_umpire_id equals u.umpire_id
                   join u1 in ipl.umpire_master on m.second_umpire_id equals u1.umpire_id
                   join p in ipl.player_master on m.man_match_player_id equals p.player_id
                   join country in ipl.country_master on m.country_id equals country.country_id

                   select new
                   {

                       m.match_id,
                       m.match_date,
                       team = t.team_id.ToString() == null ? String.Empty : t.team_name,
                       opponent = t1.team_id.ToString() == null ? String.Empty : t1.team_name,
                       toss = t2.team_id.ToString() == null ? String.Empty : t2.team_name,
                       winner = t3.team_id.ToString() == null ? String.Empty : t3.team_name,
                       first_umpire = u.umpire_id.ToString() == null ? String.Empty : u.umpire_name,
                       second_umpire = u1.umpire_id.ToString() == null ? String.Empty : u1.umpire_name,
                       man = p.player_id.ToString() == null ? String.Empty : p.player_name,
                       city = m.city_id.ToString() == null ? String.Empty : c.city_name,
                       venue = m.venue_id.ToString() == null ? String.Empty : v.venue_name,
                       country = m.country_id.ToString() == null ? String.Empty : country.country_name
                   }).ToList()
            };
            return Json(matches, JsonRequestBehavior.AllowGet);
        }

    }
}