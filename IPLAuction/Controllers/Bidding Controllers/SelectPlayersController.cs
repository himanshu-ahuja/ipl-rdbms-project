﻿using IPLAuction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPLTrial.Controllers.Bidding_Controllers
{
    public class SelectPlayersController : Controller
    {
        IPLEntities ipl = new IPLEntities();

        public ActionResult Selection()
        {
            return View();
        }

        //Get Players
        public ActionResult GetPlayers()
        {
            var season = ipl.season_master.OrderByDescending(s => s.season_year).FirstOrDefault();
            int season_id = season.season_id;
            int season_year = season.season_year;
            var bidding = ipl.bidding_master.FirstOrDefault();

            ipl.Configuration.ProxyCreationEnabled = false;

            if (bidding == null)
            {
                var player1 = new
                {
                    data =

                (from p in ipl.player_master

                 join c in ipl.country_master on p.country equals c.country_id
                 join bat in ipl.batting_style_master on p.player_batting_style equals bat.batting_id
                 join bowl in ipl.bowling_skill_master on p.player_bowling_skill equals bowl.bowling_skill_id
                 join bid in ipl.bidding_master on p.player_id equals bid.player_id into gBid

                 from bid in gBid.DefaultIfEmpty()

                 where p.selected == false

                 //|| p.active == true
                 select new
                 {
                     season = season_year,
                     p.player_name,
                     p.player_dob,
                     batting_id = bat.batting_id.ToString() == null ? String.Empty : bat.batting_style,
                     bowling_id = bowl.bowling_skill_id.ToString() == null ? String.Empty : bowl.bowling_skill,
                     c.country_name,
                     p.player_id,
                     SeasonID = season_id
                 }).ToList()
                };

                return Json(player1, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var player = new
                {
                    data =

                     (from p in ipl.player_master

                      join c in ipl.country_master on p.country equals c.country_id
                      join bat in ipl.batting_style_master on p.player_batting_style equals bat.batting_id
                      join bowl in ipl.bowling_skill_master on p.player_bowling_skill equals bowl.bowling_skill_id
                      join bid in ipl.bidding_master on p.player_id equals bid.player_id into gBid

                      from bid in gBid.DefaultIfEmpty()

                      where p.selected == false && p.player_id != bid.player_id

                  //|| p.active == true
                  select new
                      {
                          season = season_year,
                          p.player_name,
                          p.player_dob,
                          batting_id = bat.batting_id.ToString() == null ? String.Empty : bat.batting_style,
                          bowling_id = bowl.bowling_skill_id.ToString() == null ? String.Empty : bowl.bowling_skill,
                          c.country_name,
                          p.player_id,
                          SeasonID = season_id
                      }).ToList()
                };
                return Json(player, JsonRequestBehavior.AllowGet);
            }
          
        }

        //Finalize
        [HttpPost]
        public ActionResult Finalize(List<int> id)
        {
            
                if (id == null)
                {
                   
                     return Json(false, JsonRequestBehavior.AllowGet);
                }

                var season = ipl.season_master.OrderByDescending(s => s.season_year).FirstOrDefault();
                int season_id = season.season_id;
           

                foreach (int pID in id)
                {
                    var base_price = ipl.player_skills.Where(a => a.player_id == pID).FirstOrDefault();
            
                    bidding_master bidding = new bidding_master();
                    bidding.player_id = pID;
                    bidding.season_id = season_id;
                    bidding.base_price = base_price.player_base_price;
                    ipl.bidding_master.Add(bidding);
                }
                ipl.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);

        }
    }
}