﻿using IPLAuction.Models;
using IPLAuction.Models.Extended;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace IPLTrial.Controllers.Bidding_Controllers
{
  
    public class BiddingProcessController : Controller
    {
        int bid_id = 0;
        IPLEntities ipl = new IPLEntities();
        MatchUtility match_Util = new MatchUtility();
       

        public ActionResult Bidding()
        {
            // int bid_id = ipl.bidding_master.Select(a => a.bidding_id).FirstOrDefault();
            bid_id = ipl.bidding_master.Where(s => s.soldto_team_id == null).Select(a => a.bidding_id).FirstOrDefault();
            if(bid_id == 0)
            {
                return RedirectToAction("Selection","SelectPlayers");
            }
            ViewBag.bid_id = bid_id;
            ViewBag.first_bid_id = bid_id;
            ViewBag.team = match_Util.GetActiveTeam();

            var teams = ipl.team_master.Where(t => t.active == true).ToList();
            ViewBag.team_list = teams;
            ViewBag.team_count = teams.Count();

            return View();
        }

        //get team budget
        public ActionResult getTeamBalance(int id)
        {
            ipl.Configuration.ProxyCreationEnabled = false;

            var team = new
            {
                data =
               (from t in ipl.team_budget
               
                where t.team_id == id
                select new
                {
                   t.total_budget,
                   t.budget_spent
                }).ToList()
            };

            return Json(team, JsonRequestBehavior.AllowGet);
        }


        //get players for bidding
        public ActionResult GetPlayersForBidding(int id)
        {
            ipl.Configuration.ProxyCreationEnabled = false;

            ViewBag.team = match_Util.GetActiveTeam();

            if (id == 0)
            {
                int b_id = ipl.bidding_master.Where(s => s.soldto_team_id == null).Select(a => a.bidding_id).FirstOrDefault();
                id = b_id;
            }
                     
            var player = new
            {
                data =
               (from b in ipl.bidding_master
                join p in ipl.player_master on b.player_id equals p.player_id
                let years = DateTime.Now.Year - p.player_dob.Year
                let birthdayThisYear = DbFunctions.AddYears(p.player_dob, years)
                //p.player_dob.AddYears(years)
                join c in ipl.country_master on p.country equals c.country_id
                join bowl in ipl.bowling_skill_master on p.player_bowling_skill equals bowl.bowling_skill_id
                join bat in ipl.batting_style_master on p.player_batting_style equals bat.batting_id
                join s in ipl.player_skills on p.player_id equals s.player_id
                where b.bidding_id == id && b.soldto_team_id == null
                select new
                {
                    b.bidding_id,
                    p.player_id,
                    name = p.player_name,
                    c.country_name,
                    Age = birthdayThisYear > DateTime.Now ? years - 1 : years,
                    batting_style = bat.batting_id.ToString() == null ? String.Empty : bat.batting_style,
                    bowling_skill = bowl.bowling_skill_id.ToString() == null ? String.Empty : bowl.bowling_skill,
                    batting_star = s.player_skill_id.ToString() == null ? 0 : s.player_batting_star,
                    bowling_star = s.player_skill_id.ToString() == null ? 0 : s.player_bowling_star,
                    base_price = s.player_base_price,
                    image = p.player_image
                }).ToList()
            };

            //ViewBag.bid_id = id;

            return Json(player, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult StartBidding(int id)
        {
            ViewBag.team = match_Util.GetActiveTeam();

            bool status = true;
            using (IPLEntities i = new IPLEntities())
            {
                int player_id = (int)i.bidding_master.Where(b => b.bidding_id == id).Select(p => p.player_id).FirstOrDefault();
                if(player_id != 0)
                {
                    var change_status = i.bidding_master.Where(p => p.player_id == player_id).FirstOrDefault();
                    change_status.inprocess = status;
                    i.SaveChanges();
                    return Json(status, JsonRequestBehavior.AllowGet);
                }

            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PlayerUnsold(int id)
        {
            ViewBag.team = match_Util.GetActiveTeam();

            bool status = false;
            using (IPLEntities i = new IPLEntities())
            {
                int player_id = (int)i.bidding_master.Where(b => b.bidding_id == id).Select(p => p.player_id).FirstOrDefault();
                if (player_id != 0)
                {
                    i.bidding_master.Where(p => p.player_id == player_id).FirstOrDefault().sold_status = status;
                    i.SaveChanges();
                    return Json(status, JsonRequestBehavior.AllowGet);
                }

            }
            status = true;
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PopulateTeamsForBidding()
        {
            var teams = new
            {
                data =
                    (from t in ipl.team_master
                     where t.active == true
                     select new
                     {
                        team_id = t.team_id,
                        team_name = t.team_name

                     }).ToList()
            };

            return Json(teams, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PurchasePlayer(int id, decimal base_price, decimal price, int t_id)
        {
            bool status = false;
            var team = ipl.team_budget.Where(t => t.team_id == t_id).FirstOrDefault();

            var remain_balance = team.total_budget - team.budget_spent;
                if(remain_balance < price)
            {
              
                return new JsonResult { Data = new { status = status } };
            }

            if (price < base_price)
            {
               
                return new JsonResult { Data = new { status = status } };
            }

            var bid_details = ipl.bidding_master.Where(b => b.bidding_id == id).FirstOrDefault();
            bid_details.soldto_team_id = t_id;
            bid_details.base_price = base_price;
            bid_details.sold_price = price;
            bid_details.sold_status = true;

            team_squad t_squad = new team_squad();
            t_squad.player_id = bid_details.player_id;
            t_squad.season_id = (int)bid_details.season_id;
            t_squad.team_id = t_id;
            ipl.team_squad.Add(t_squad);

            var team_name = ipl.team_master.Where(t => t.team_id == t_id).FirstOrDefault();


            var p_master = ipl.player_master.Where(p => p.player_id == bid_details.player_id).FirstOrDefault();
            p_master.selected = true;

            var new_budget = team.budget_spent + price;
            team.budget_spent = new_budget;

            //ipl.bidding_master.Add(bid_details);
            ipl.SaveChanges();

            status = true;

            ViewBag.Success = true;


            //try
            //{
            //    if (ModelState.IsValid)
            //    {
            //        var senderemail = new MailAddress("ahujaheman@gmail.com", "ahujaheman@gmail.com");
            //        var receiveremail = new MailAddress("ahujaheman@gmail.com", "ahujaheman@gmail.com");

            //        var password = "excision@#941";
            //        var sub = "IPL Auction Player Purchased '"+team_name.team_name+"'";
            //        var body = "You have purchased '"+ p_master.player_name+"' Successfully for '"+price +"'. Remaining team fund is '"+remain_balance+"'. For any queries please contact the administration team ";

            //        var smtp = new SmtpClient
            //        {
            //            Host = "smtp.gmail.com",
            //            Port = 587,
            //            EnableSsl = true,
            //            DeliveryMethod = SmtpDeliveryMethod.Network,
            //            UseDefaultCredentials = false,
            //            Credentials = new NetworkCredential(senderemail.Address, password)

            //        };
            //        using (var mess = new MailMessage(senderemail, receiveremail)
            //        {
            //            Subject = sub,
            //            Body = body
            //        }
            //            )
            //        {
            //            smtp.Send(mess);
            //        }
            //    }
            //    //TempData["success"] = "Email Sent!";
            //}
            //catch (Exception )
            //{
            //    Console.WriteLine("Email not sent");
            //    //TempData["message"] = "Email is not registered!";
            //}




            //var bidding = new
            //{
            //    data =
            //       (from t in ipl.bidding_master
            //        join p in ipl.player_master on t.player_id equals p.player_id
            //        join te in ipl.team_master on t.soldto_team_id equals te.team_id
            //        where t.bidding_id == bid_details.bidding_id
            //        select new
            //        {
            //            player_name = p.player_name,
            //            base_price = t.base_price,
            //            team_name = te.team_name,
            //            sold_price = t.sold_price,

            //        }).ToList()
            //};


            //return Json(bidding, JsonRequestBehavior.AllowGet);

            return new JsonResult { Data = new { status = status } };


        }
    }
}