﻿using IPLAuction.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace IPLAuction.Controllers
{
    public class HomeController : Controller
    {
        IPLEntities ipl = new IPLEntities();
        public ActionResult Admin_Home()
        {
            Response.Cache.SetNoStore();
            ViewBag.count_matches = ipl.match_master.ToList().Count;
            ViewBag.count_players = ipl.player_master.ToList().Count;
            ViewBag.count_teams = ipl.team_master.ToList().Count;
            ViewBag.active_teams = ipl.team_master.Where(a => a.active == true).ToList().Count();
            ViewBag.coaches_count = ipl.coach_master.ToList().Count();
            ViewBag.umpires_count = ipl.umpire_master.ToList().Count();
            ViewBag.total_budget = ipl.team_budget.Select(a => a.total_budget).Sum();

            return View();
        }
        //Logout
        public ActionResult Logout()
        {
            Session.RemoveAll();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            return RedirectToAction("IPL_Login", "Login");
        }

    }
}